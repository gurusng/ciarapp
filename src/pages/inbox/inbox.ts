import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {MessagePage} from '../message/message';
import {UsersProvider} from "../../providers/users/users";

/**
 * Generated class for the InboxPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-inbox',
    templateUrl: 'inbox.html',
})
export class InboxPage {

    loading:boolean = false;
    messageUsers:any[] = [];

    constructor(public navCtrl: NavController,
                public userProvider: UsersProvider,
                public navParams: NavParams) {
    }

    ionViewDidLoad() {
        this.getMessageUsers()
        console.log('ionViewDidLoad InboxPage');
    }

    newMessage() {
        this.navCtrl.push(MessagePage);
    }

    getMessageUsers(){
        let data = {
            'cid' : this.userProvider.user.cid,
        };

        this.userProvider.messageUsers(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                console.log(success);
                this.messageUsers = success.data;
            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            console.log(JSON.stringify(error))
        });

    }

}
