import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {OrdernotesPage} from '../ordernotes/ordernotes';
import {UsersProvider} from "../../providers/users/users";

/**
 * Generated class for the ShoppingcartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-shoppingcart',
    templateUrl: 'shoppingcart.html',
})
export class ShoppingcartPage {

    loading:boolean = false;
    cart:any[];

    constructor(public navCtrl: NavController,
                public userProvider: UsersProvider,
                public navParams: NavParams) {
    }

    ionViewWillEnter(){
        this.getCart();
    }

    getCart(){
        let data = {
            'cid' : this.userProvider.user.cid,
        };

        this.loading = true;
        this.userProvider.cart(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.cart = success.data;
                console.log(success);


            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            console.log(JSON.stringify(error))
        });

    }

    deleteFromCart(cart){
        let data = {
            'cid' : this.userProvider.user.cid,
            'cartid' : cart.cartid
        };
        this.loading = true;

        this.userProvider.deleteFromCart(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.userProvider.showToast("Removed from cart.");
                this.getCart();

            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            console.log(JSON.stringify(error))
        });


    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ShoppingcartPage');
    }

    goToOrderNotes() {
        this.navCtrl.push(OrdernotesPage);
    }



}
