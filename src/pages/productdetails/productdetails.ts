import {Component} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {ShoppingcartPage} from '../shoppingcart/shoppingcart';
import {CustomizePage} from '../customize/customize';
import {UsersProvider} from "../../providers/users/users";
import {SelectMoodPage} from "../select-mood/select-mood";
import {ReviewsPage} from "../reviews/reviews";
import {CommentsPage} from "../comments/comments";

/**
 * Generated class for the ProductdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-productdetails',
    templateUrl: 'productdetails.html',
})
export class ProductdetailsPage {

    loading: boolean = false;
    likeLoading: boolean = false;
    post: any;
    recentComments:any[] = [];
    recentReviews:any[] = [];
    comments: string[] = [];
    reviews: string[] = [];
    comment: string;

    constructor(public navCtrl: NavController,
                public modalCtrl: ModalController,
                private userProvider: UsersProvider,
                public navParams: NavParams) {
        this.post = this.navParams.get('post');
    }


    ionViewDidLoad() {
        this.comments = this.post.comments.slice(0,5);
        this.reviews = this.post.reviews.slice(0,5);

        console.log('ionViewDidLoad ProductdetailsPage');
    }

    goToCart() {
        this.navCtrl.push(ShoppingcartPage);
    }

    customize() {
        this.navCtrl.push(CustomizePage,{post : this.post});
    }


    saveToMood(pid) {
        console.log(JSON.stringify(pid));
        const modal = this.modalCtrl.create(SelectMoodPage, {'pid': pid});
        modal.present();
    }


    like(pid) {
        let data = {
            'cid': this.userProvider.user.cid,
            'pid': pid
        };

        this.likeLoading = true;
        this.userProvider.like(data).subscribe((success: any) => {
            this.likeLoading = false;
            if (success.message == "Successful") {
                this.post.isLiked = true;
                // this.userProvider.showAlert("Success", "Liked");

            } else {
                this.userProvider.showAlert("Error", success.message);
            }
        }, (error) => {
            console.log(error)
        });
    }


    unlike(pid) {
        let data = {
            'cid': this.userProvider.user.cid,
            'pid': pid
        };

        this.likeLoading = true;
        this.userProvider.unlike(data).subscribe((success: any) => {
            this.likeLoading = false;
            if (success.message == "Successful") {
                this.userProvider.showAlert("Success", "Un-liked");
                this.post.isLiked = false;
            } else {
                this.userProvider.showAlert("Error", success.message);
            }
        }, (error) => {
            console.log(error)
        });
    }


    addComment() {
        this.navCtrl.push(CommentsPage,{post: this.post});
    }

    addReview() {
        this.navCtrl.push(ReviewsPage, {post: this.post});
    }

}
