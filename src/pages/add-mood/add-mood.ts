import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {UsersProvider} from "../../providers/users/users";
import {isUndefined} from "ionic-angular/es2015/util/util";

/**
 * Generated class for the AddMoodPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-mood',
  templateUrl: 'add-mood.html',
})
export class AddMoodPage {

    loading:boolean = false;
    public name:string;
    public description:string;

    constructor(public navCtrl: NavController,
                private userProvider : UsersProvider,
                public viewCtrl: ViewController,
                public navParams: NavParams) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad AddMoodPage');
    }

    dismiss(){
        this.viewCtrl.dismiss();
    }

    saveMood(){
        let data = {
            'cid'  : this.userProvider.user.cid,
            'name' : this.name,
            'description' : this.description
        };
        this.loading = true;


        this.userProvider.addMood(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.userProvider.showAlert("Success","Mood Added");
                this.viewCtrl.dismiss();

            } else {
                this.userProvider.showAlert("Error",success.message);
            }
        },(error)=>{this.loading = false;console.log(error)});
    }



}
