import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddMoodPage } from './add-mood';

@NgModule({
  declarations: [
    AddMoodPage,
  ],
  imports: [
    IonicPageModule.forChild(AddMoodPage),
  ],
})
export class AddMoodPageModule {}
