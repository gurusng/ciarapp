import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {HomePage} from '../home/home';
import {UsersProvider} from "../../providers/users/users";
import {TabsPage} from "../tabs/tabs";
import  {Storage} from "@ionic/storage";

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-signup',
    templateUrl: 'signup.html',
})
export class SignupPage {

    username: string = 'username';
    name: string = 'test';
    brandName: string = 'testing';
    businessName: string = 'testing';
    phone: string = '67389452323';
    email: string = 'testing@yopmail.com';
    password: string = '12';
    confirmPassword: string = '12';
    categories: string;
    methodOfID: string;
    facebook: string = 'facebook';
    instagram: string = 'insta';
    twitter: string = 'twitter';
    website: string = 'website';
    loading: boolean = false;
    image: string;
    type: string = 'User';

    constructor(public navCtrl: NavController,
                public userProvider : UsersProvider,
                private storage: Storage,
                public navParams: NavParams) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SignupPage');
    }

    home() {
        this.navCtrl.push(HomePage);
    }

    signUp(){

        let data = {
            'username' : this.username,
            'name' : this.name,
            'brandName' : this.brandName,
            'businessName' : this.businessName,
            'phone' : this.phone,
            'email' : this.email,
            'categories' : this.categories,
            'methodOfID' : this.methodOfID,
            'facebook' : this.facebook,
            'instagram' : this.instagram,
            'twitter' : this.twitter,
            'website' : this.website,
            'password' : this.password
        };

        if(this.confirmPassword !== this.password) {
            this.password = null;
            this.confirmPassword = null;
            this.userProvider.showAlert("Error","Passwords don't match. Please try again");
            return;
        }

        this.loading = true;

        this.userProvider.signUpUser(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.storage.set('isSignedIn',true);
                this.storage.set('user',success.data);
                this.userProvider.user = success.data;
                this.userProvider.showAlert("Success", "You have been registered.");
                this.navCtrl.setRoot(TabsPage);

            }
            else {
                this.userProvider.showAlert("Error", success.message);
            }
            console.log(JSON.stringify(success));

        },(error)=>{
            this.loading = false;
            console.log(JSON.stringify(error));
        });

    }


}
