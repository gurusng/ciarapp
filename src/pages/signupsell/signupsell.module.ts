import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupsellPage } from './signupsell';

@NgModule({
  declarations: [
    SignupsellPage,
  ],
  imports: [
    IonicPageModule.forChild(SignupsellPage),
  ],
})
export class SignupsellPageModule {}
