import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {HomePage} from '../home/home';
import {UsersProvider} from "../../providers/users/users";
import {SigninPage} from "../signin/signin";
import {Storage} from "@ionic/storage";

/**
 * Generated class for the SignupsellPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-signupsell',
    templateUrl: 'signupsell.html',
})
export class SignupsellPage {

    username: string = 'username';
    name: string = 'test';
    brandName: string = 'testing';
    businessName: string = 'testing';
    phone: string = '67389452323';
    email: string = 'testing@yopmail.com';
    password: string = '12';
    confirmPassword: string = '12';
    categories: string;
    methodOfID: string;
    facebook: string = 'facebook';
    instagram: string = 'insta';
    twitter: string = 'twitter';
    website: string = 'website';
    loading: boolean = false;
    image: string;
    type: string = 'Seller';

    constructor(public navCtrl: NavController,
                public userProvider: UsersProvider,
                private storage: Storage,
                public navParams: NavParams) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SignupsellPage');
    }

    home() {
        this.navCtrl.push(HomePage);
    }

    removeID(){
        this.image = null;
    }

    // takePhoto() {
    //
    //     const options: CameraOptions = {
    //         quality: 100,
    //         saveToPhotoAlbum: false,
    //         destinationType: this.camera.DestinationType.FILE_URI,
    //         encodingType: this.camera.EncodingType.JPEG,
    //         mediaType: this.camera.MediaType.PICTURE,
    //         correctOrientation: true,
    //         targetWidth: 800,
    //         targetHeight: 800
    //     };
    //
    //     this.camera.getPicture(options).then((imagePath) => {
    //
    //         // let currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
    //         // let correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
    //
    //         console.log(imagePath);
    //
    //
    //         this.image = imagePath;
    //
    //
    //
    //     }, (error) => {
    //         console.log(error)
    //         // Handle error
    //     });
    // }


    // signUp(){
    //     console.log(this.categories);
    //
    //     if(this.confirmPassword !== this.password) {
    //         this.password = null;
    //         this.confirmPassword = null;
    //         this.userProvider.showAlert("Error","Passwords don't match. Please try again");
    //         return;
    //     }
    //
    //     this.loading = true;
    //
    //
    //     const fileTransfer: FileTransferObject = this.transfer.create();
    //
    //
    //     let options:any = {
    //         fileKey: "id",
    //         fileName: "image.jpg",
    //         chunkedMode: false,
    //         mimeType: "multipart/form-data",
    //         params : {
    //             'username' : this.username,
    //             'name' : this.name,
    //             'brandName' : this.brandName,
    //             'businessName' : this.businessName,
    //             'phone' : this.phone,
    //             'email' : this.email,
    //             'cat' : this.categories,
    //             'methodOfID' : this.methodOfID,
    //             'facebook' : this.facebook,
    //             'instagram' : this.instagram,
    //             'twitter' : this.twitter,
    //             'website' : this.website,
    //             'password' : this.password
    //         },
    //     };
    //
    //
    //     fileTransfer.upload(this.image, this.userProvider.url + 'sign-up' , options)
    //         .then((response:any) => {
    //             // success
    //             let success = JSON.parse(response.response);
    //             this.loading = false;
    //             if(success.message == "Successful"){
    //                 this.userProvider.showAlert("Success", "Your registration has been sent for approval. " +
    //                     "When approved, you can login.");
    //                 this.navCtrl.push(SigninPage);
    //             }
    //             else {
    //
    //                 this.userProvider.showAlert("Error", success.message);
    //             }
    //             console.log(JSON.stringify(success.response));
    //
    //         }, (error) => {
    //             this.loading = false;
    //             console.log(JSON.stringify(error));
    //
    //         })
    //
    // }

    checkRequired() {

        let form = document.getElementById("form");

        for (let i = 0; i < form.querySelectorAll("input").length; i++) {

            if (form.querySelectorAll("input")[i].required == true &&
                form.querySelectorAll("input")[i].value == ""
            ) {

                this.userProvider.showAlert("Error", form.querySelectorAll("input")[i].name + " is required.");
                return false;
            }

            return true; //all is well. Continue
        }
    }


}
