import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {UsersProvider} from "../../providers/users/users";

/**
 * Generated class for the SelectMoodPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-select-mood',
  templateUrl: 'select-mood.html',
})
export class SelectMoodPage {

    mood:any;
    moods:string[];
    loading:boolean = false;
    pid :string;

    constructor(public navCtrl: NavController,
              private userProvider : UsersProvider,
              public viewCtrl : ViewController,
              public navParams: NavParams) {

       // this.pid =  this.viewCtrl.data.pid;
        console.log(JSON.stringify(this.navParams.get('pid')));
        this.pid = this.navParams.get('pid');
    }

    ionViewDidEnter(){
        this.getMoods();
    }

    ionViewDidLoad() {
    console.log('ionViewDidLoad SelectMoodPage');
    }

    dismiss(){
        this.viewCtrl.dismiss();
    }

    getMoods(){
        let data ={
            'cid' : this.userProvider.user.cid
        };

        this.loading = true;
        this.userProvider.mood(data).subscribe((success:any)=>{
            this.loading = false;

            if(success.message = "Successful"){
                this.moods = success.data;
                console.log(success.data);
            } else {
                this.userProvider.showAlert("Error",success.message);
            }
        },(error)=>{
            console.log(error);

            this.loading = false;
        });
    }

    moodSelected(mood){

        this.mood = mood;
        this.addMoodPost();
    }

    addMoodPost(){
        let data = {
            'moid' : this.mood.moid,
            'pid'  : this.pid
        };

        this.userProvider.addMoodPost(data).subscribe((success:any)=>{
            if(success.message == "Successful"){
                this.userProvider.showAlert("Success", "Post added to your mood board.");
                this.dismiss();
            }
            else
                this.userProvider.showAlert("Error", success.message);

        },(error)=>{console.log(error)});
    }

}
