<<<<<<< HEAD
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ShoppingcartPage } from '../shoppingcart/shoppingcart';
=======
import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {UsersProvider} from "../../providers/users/users";
import {ShoppingcartPage} from "../shoppingcart/shoppingcart";
>>>>>>> 43c85df142951eebb9a1203bc39a86e2fff1589c

/**
 * Generated class for the CustomizePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-customize',
    templateUrl: 'customize.html',
})
export class CustomizePage {

    post: any;
    sizes: any[];
    colors: any[];
    quantity: number;
    size:any;
    color:any;
    options:any[];
    loading:boolean = false;
    selectedOptions:any[] =[];

    constructor(public navCtrl: NavController,
                public userProvider: UsersProvider,
                public navParams: NavParams) {


        this.post = this.navParams.get('post');


        this.sizes = this.post.sizes.split(',');
        this.colors = this.post.colors.split(',');

    }


    ionViewDidLoad() {

        console.log(this.post);
        console.log(this.post.options);

        this.options = JSON.parse(this.post.options);
        this.selectedOptions = JSON.parse(this.post.options);

    }

    addToCart(){

        let data = {
            'cid' : this.userProvider.user.cid,
            'pid' : this.post.pid,
            'size' : this.size,
            'color' : this.color,
            'options' : JSON.stringify(this.selectedOptions),
            'quantity' : this.quantity

        };

        this.loading = true;

        this.userProvider.addToCart(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){

                console.log(success);
                this.userProvider.showToast("Added to cart");
                this.navCtrl.push(ShoppingcartPage);


            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            console.log(JSON.stringify(error))
        });

    }

<<<<<<< HEAD
  ionViewDidLoad() {
    console.log('ionViewDidLoad CustomizePage');
  }
  addToCart(){
    this.navCtrl.push(ShoppingcartPage);
  }
=======
>>>>>>> 43c85df142951eebb9a1203bc39a86e2fff1589c

}
