import {Component} from '@angular/core';
import {HomePage} from "../home/home";
import {ProductsearchPage} from "../productsearch/productsearch";
import {MoodPage} from "../mood/mood";


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

    tab1Root = HomePage;
    tab2Root = ProductsearchPage;
    tab3Root = MoodPage;
    tab4Root = MoodPage;


    constructor() {

    }

    ionViewWillEnter(){
    }


}
