import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MoodItemsPage } from './mood-items';

@NgModule({
  declarations: [
    MoodItemsPage,
  ],
  imports: [
    IonicPageModule.forChild(MoodItemsPage),
  ],
})
export class MoodItemsPageModule {}
