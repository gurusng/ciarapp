<<<<<<< HEAD
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AddcardPage } from '../addcard/addcard';

import { OrdernotesPage } from '../ordernotes/ordernotes';
=======
import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {AddcardPage} from '../addcard/addcard';
import {SuccessPage} from '../success/success';
import {UsersProvider} from "../../providers/users/users";
import {HomePage} from "../home/home";
>>>>>>> 43c85df142951eebb9a1203bc39a86e2fff1589c

/**
 * Generated class for the PaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-payment',
    templateUrl: 'payment.html',
})
export class PaymentPage {

<<<<<<< HEAD
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentPage');
  }
  goToAddCard(){
    this.navCtrl.push(AddcardPage);
  };
  goToOrderNotes(){
    this.navCtrl.push(OrdernotesPage);
  }
=======
    order:any;
    loading:boolean = false;
    cards:any[];
    card:any;
    cardid:number;


    constructor(public navCtrl: NavController,
                public userProvider: UsersProvider,
                public navParams: NavParams) {
        this.order = this.navParams.get("order");

        this.getCards();
    }

    ionViewDidLoad() {
        this.getCards();
        console.log('ionViewDidLoad PaymentPage');
    }

    goToAddCard() {
        this.navCtrl.push(AddcardPage);
    }

    goToSuccess() {
        this.navCtrl.push(SuccessPage);
    }

    goToHome(){
        this.navCtrl.setRoot(HomePage);
    }

    selectCard(card){
        this.cardid = card.cardid;
    }


    pay(){
        let data = {
            'cardid' : this.cardid,
            'amount' : this.order.total,
            'cid'    : this.userProvider.user.cid,
            'oid'    : this.order.oid
        };

        this.loading = true;
        this.userProvider.pay(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.userProvider.showToast("Payment Complete");
                this.userProvider.goToHome();

            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            console.log(JSON.stringify(error))
        });

    }

    getCards(){
        let data = {
            'cid' : this.userProvider.user.cid,
        };
        this.loading = true;
        this.userProvider.cards(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){

                this.cards = success.data.cards;

            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            console.log(JSON.stringify(error))
        });

    }

>>>>>>> 43c85df142951eebb9a1203bc39a86e2fff1589c

}
