import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {UsersProvider} from "../../providers/users/users";
import {InboxPage} from "../inbox/inbox";

/**
 * Generated class for the ReviewsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-reviews',
    templateUrl: 'reviews.html',
})
export class ReviewsPage {

    review: string;
    reviews:any[];
    post: any;

    loading:boolean = false;

    constructor(public navCtrl: NavController,
                private userProvider : UsersProvider,
                public navParams: NavParams) {
       this.post = this.navParams.get("post");
       this.reviews = this.post.reviews;
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ReviewsPage');
    }

    goToMessages() {
        this.navCtrl.push(InboxPage);
    }

    addReview(){
        let data = {
            'cid' : this.userProvider.user.cid,
            'pid' : this.post.pid,
            'review' : this.review
        };

        this.userProvider.addReview(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.review = "";

                this.userProvider.postDetails(data).subscribe((success1:any)=>{
                    this.post = success1.data;
                    this.reviews = success1.data.reviews;
                    console.log(success1);
                },(error)=>{
                    this.loading = false;
                    console.log(JSON.stringify(error))
                });



            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            console.log(JSON.stringify(error))
        });

    }
}
