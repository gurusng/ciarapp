import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrdernotesPage } from './ordernotes';

@NgModule({
  declarations: [
    OrdernotesPage,
  ],
  imports: [
    IonicPageModule.forChild(OrdernotesPage),
  ],
})
export class OrdernotesPageModule {}
