<<<<<<< HEAD
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PaymentPage } from '../payment/payment';
import { SuccessPage } from '../success/success';
=======
import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {PaymentPage} from '../payment/payment';
import {UsersProvider} from "../../providers/users/users";
import {TabsPage} from "../tabs/tabs";
>>>>>>> 43c85df142951eebb9a1203bc39a86e2fff1589c

/**
 * Generated class for the OrdernotesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-ordernotes',
    templateUrl: 'ordernotes.html',
})
export class OrdernotesPage {

    notes:string;
    loading:boolean = false;

    constructor(public navCtrl: NavController,
                public userProvider: UsersProvider,
                public navParams: NavParams) {
    }

<<<<<<< HEAD
  goToSuccess(){
    this.navCtrl.push(SuccessPage);
  }
=======
    ionViewDidLoad() {
        console.log('ionViewDidLoad OrdernotesPage');
    }
>>>>>>> 43c85df142951eebb9a1203bc39a86e2fff1589c

    goToPayment() {
        this.navCtrl.push(PaymentPage);
    }

    goToHome(){
        this.navCtrl.setRoot(TabsPage);
    }

    addOrder(){
        let data = {
            'cid' : this.userProvider.user.cid,
            'notes' : this.notes
        };

        this.loading = true;

        this.userProvider.addOrder(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.userProvider.showToast("Order Placed");
                this.navCtrl.push(PaymentPage,{order : success.data});

            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            console.log(JSON.stringify(error))
        });

    }
}
