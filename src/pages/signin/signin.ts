import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {SignupPage} from '../signup/signup';
import {SignupsellPage} from '../signupsell/signupsell';
import {ICustomer} from "../../interfaces/customer.interface";
import {UsersProvider} from "../../providers/users/users";
import {TabsPage} from "../tabs/tabs";
import {Storage} from "@ionic/storage";

/**
 * Generated class for the SigninPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-signin',
    templateUrl: 'signin.html',
})
export class SigninPage {


    email: string = 'promise@gmail.com';
    password: string = '123456';
    loading: boolean = false;

    constructor(public navCtrl: NavController,
                private userProvider: UsersProvider,
                private storage: Storage,
                public navParams: NavParams) {


        this.storage.get('isSignedIn').then((success:any)=>{
            if(success == true){
                this.storage.get('user').then((success)=>{
                    this.userProvider.user = success;
                    this.navCtrl.setRoot(TabsPage);
                });
            }
        });


    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SigninPage');
    }

    checkRequired() {

        let form = document.getElementById("form");

        for (let i = 0; i < form.querySelectorAll("input").length; i++) {

            if (form.querySelectorAll("input")[i].required == true &&
                form.querySelectorAll("input")[i].value == ""
            ) {

                this.userProvider.showAlert("Error", form.querySelectorAll("input")[i].name + " is required.");
                return false;
            }

            return true; //all is well. Continue
        }
    }


    signIn() {


        if (this.checkRequired() == false) return;

        let user = new ICustomer();
        user.email = this.email;
        user.password = this.password;

        this.loading = true;
        this.userProvider.signIn(user).subscribe((success: any) => {

            this.loading = false;
            if (success.message == "Successful") {
                this.storage.set('isSignedIn', true);
                this.storage.set('user', success.data);
                this.userProvider.user = success.data;
                this.navCtrl.setRoot(TabsPage);
            } else {

                console.log(JSON.stringify(success));
                this.userProvider.showAlert('Error', success.message);
            }


        }, (error) => {
            this.loading = false;
            console.log(JSON.stringify(error));
            this.userProvider.showAlert('Error', error.message);
        });


    }

    user() {
        this.navCtrl.push(SignupPage);
    }

    seller() {
        this.navCtrl.push(SignupsellPage);
    }

}
