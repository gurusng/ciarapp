import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductsearchPage } from './productsearch';

@NgModule({
  declarations: [
    ProductsearchPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductsearchPage),
  ],
})
export class ProductsearchPageModule {}
