import {Component} from '@angular/core';
import {ModalController, NavController, ActionSheetController, Platform} from 'ionic-angular';
import {MessagePage} from '../message/message';
import {ProductdetailsPage} from '../productdetails/productdetails';
import {CommentsPage} from '../comments/comments';
import {InboxPage} from '../inbox/inbox';
import {ProductsearchPage} from '../productsearch/productsearch';
import {UsersProvider} from "../../providers/users/users";
import {isUndefined} from "ionic-angular/es2015/util/util";
import {SelectMoodPage} from "../select-mood/select-mood";
import {ReviewsPage} from "../reviews/reviews";
import {ShoppingcartPage} from "../shoppingcart/shoppingcart";


@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    loading: boolean = false;
    likeLoading: boolean = false;
    posts: any[] = [];
    section: any = "comments";
    moods: string[];


    refresher: any;

    constructor(public navCtrl: NavController,
                private platform: Platform,
                public modalCtrl: ModalController,
                public actionSheetCtrl: ActionSheetController,
                public userProvider: UsersProvider
    ) {

        platform.ready().then(() => {

            try {
                window["plugins"].OneSignal.sendTag("user_id", this.userProvider.user.cid);
                window["plugins"].OneSignal.sendTag("user_name", this.userProvider.user.username);

            }catch (e) {

            }

        });


    }


    options(){
        let actionSheet = this.actionSheetCtrl.create({
            title: "",
            buttons: [
                {
                    text: 'Message',
                    handler: () => {

                    }
                },{

                    text: 'Share',
                    handler: () => {

                    }
                },{
                    text: 'Report',
                    handler: () => {

                    }
                },{

                    text: 'Block',
                    handler: () => {

                    }
                },{
                    text: 'Close Menu',
                    role: 'cancel',
                    handler: () => {
                        //
                    }
                }
            ]
        });
        actionSheet.present();
    }


    goToDetails(post) {
        this.navCtrl.push(ProductdetailsPage,{'post' : post})
    }

    goToMessages() {
        this.navCtrl.push(InboxPage);
    }

    goToComments(post) {
        this.navCtrl.push(CommentsPage,{post: post});
    }

    goToReviews(post) {
        this.navCtrl.push(ReviewsPage,{ 'post' : post});
    }

    goToProductSearch() {
        this.navCtrl.push(ProductsearchPage);
    }

    refresh(refresher) {
        this.userProvider.canGoBack = false;
        this.userProvider.previousPage = null;

        this.refresher = refresher;
        this.getPosts();
    }


    ionViewDidEnter() {
        this.userProvider.canGoBack = false;
        this.userProvider.previousPage = null;
        this.getPosts();
    }

    saveToMood(pid) {
        console.log(JSON.stringify(pid));
        const modal = this.modalCtrl.create(SelectMoodPage, {'pid': pid});
        modal.present();
    }
    //
    // goToDetails(post) {
    //     this.userProvider.changeCurrentPage("PostDetailsPage");
    //     this.navCtrl.push(PostDetailsPage, {'post': post});
    // }

    getPosts() {

        if (!isUndefined(this.refresher)) this.refresher.complete();

        this.loading = true;

        let data = {
            'cid': this.userProvider.user.cid
        };

        this.userProvider.timeline(data).subscribe((success: any) => {

            this.loading = false;
            this.posts = success.data.reverse();

            console.log(JSON.stringify(success.data));

        }, (error) => {
            console.log(JSON.stringify(error));
            this.loading = false;
        });
    }

    like(pid) {
        let data = {
            'cid': this.userProvider.user.cid,
            'pid': pid
        };

        this.likeLoading = true;
        this.userProvider.like(data).subscribe((success: any) => {
            this.likeLoading = false;
            if (success.message == "Successful") {
                this.getPosts();
            } else {
                this.userProvider.showAlert("Error", success.message);
            }
        }, (error) => {
            console.log(error)
        });
    }

    unlike(pid) {
        let data = {
            'cid': this.userProvider.user.cid,
            'pid': pid
        };

        this.likeLoading = true;
        this.userProvider.unlike(data).subscribe((success: any) => {
            this.likeLoading = false;
            if (success.message == "Successful") {
                this.userProvider.showAlert("Success", "Un-liked");
                this.getPosts();
            } else {
                this.userProvider.showAlert("Error", success.message);
            }
        }, (error) => {
            console.log(error)
        });
    }

    goToCart(){
        this.navCtrl.push(ShoppingcartPage);
    }


}
