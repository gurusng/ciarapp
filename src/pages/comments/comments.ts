import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {InboxPage} from '../inbox/inbox';
import {UsersProvider} from "../../providers/users/users";

/**
 * Generated class for the CommentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-comments',
    templateUrl: 'comments.html',
})
export class CommentsPage {

    comment: string;
    comments:any[];
    post: any;

    loading:boolean = false;

    constructor(public navCtrl: NavController,
                private userProvider: UsersProvider,
                public navParams: NavParams) {
        this.post = this.navParams.get("post");
        this.comments = this.post.comments;
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad CommentsPage');
    }

    goToMessages() {
        this.navCtrl.push(InboxPage);
    }

    addComment() {
        let data = {
            'cid': this.userProvider.user.cid,
            'pid': this.post.pid,
            'comment': this.comment
        };

        this.userProvider.makeComment(data).subscribe((success: any) => {
            this.loading = false;
            if (success.message == "Successful") {
                this.comment = "";

                this.userProvider.postDetails(data).subscribe((success1: any) => {
                    this.post = success1.data;
                    this.comments = success1.data.comments;
                    console.log(success1);
                }, (error) => {
                    this.loading = false;
                    console.log(JSON.stringify(error))
                });


            } else {
                this.userProvider.showAlert("Error", success.message);
            }

        }, (error) => {
            this.loading = false;
            console.log(JSON.stringify(error))
        });

    }

}
