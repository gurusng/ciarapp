import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {PaymentPage} from '../payment/payment';
import {UsersProvider} from "../../providers/users/users";
import {ShoppingcartPage} from "../shoppingcart/shoppingcart";

/**
 * Generated class for the AddcardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-addcard',
    templateUrl: 'addcard.html',
})
export class AddcardPage {

    loading:boolean = false;
    cardNumber: string = "5399838383838381";
    cvv: string = "470";
    expiryMonth: string = "10";
    expiryYear: string = "22";
    pin: string = "3310";
    reference: string;
    paymentMessage: string;

    showOtp = false;
    otp = "";


    constructor(public navCtrl: NavController,
                public userProvider: UsersProvider,
                public navParams: NavParams) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad AddcardPage');
    }

    goToPayment() {
        this.navCtrl.push(PaymentPage);
    }


    addCard(){

        let data = {
            'cid'        : this.userProvider.user.cid,
            'cardNumber' : this.cardNumber,
            'cvv'        : this.cvv,
            'expiryMonth': this.expiryMonth,
            'expiryYear' : this.expiryYear,
            'pin'        : this.pin

        };

        this.loading = true;
        this.userProvider.addCard(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                console.log(success);
                this.reference = success.data.reference;
                this.paymentMessage = success.data.message;
                this.showOtp = true;


            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            console.log(JSON.stringify(error))
        });

    }

    authorizePayment(){
        console.log(this.otp);
        console.log(this.reference);
        let data = {
            'reference' : this.reference,
            'otp'       : this.otp
        };

        this.loading = true;

        this.userProvider.authorizePayment(data).subscribe((success:any)=>{
            this.loading = false;
            if(success.message == "Successful"){
                this.userProvider.showToast("Card Added");
                this.userProvider.customerUpdatedDetails();
                this.navCtrl.push(ShoppingcartPage);


            } else{
                this.userProvider.showAlert("Error",success.message);
            }

        },(error)=>{
            this.loading = false;
            console.log(JSON.stringify(error))
        });

    }



}
