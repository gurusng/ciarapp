import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';



import {SigninPage} from "../pages/signin/signin";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = SigninPage ;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();



        var notificationOpenedCallback = function (jsonData) {
            console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
        };

        window["plugins"].OneSignal
            .startInit("02fe739e-6ead-422c-b59f-186e1072216b", "541279477727")
            .handleNotificationOpened(notificationOpenedCallback)
            .endInit();

        // register app uid with onesignal's
        // register username with onesignal's

    });
  }
}

