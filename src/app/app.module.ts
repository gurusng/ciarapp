import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {SigninPage} from '../pages/signin/signin';
import {SignupPageModule} from '../pages/signup/signup.module';
import {SignupsellPageModule} from '../pages/signupsell/signupsell.module';
import {CommentsPageModule} from '../pages/comments/comments.module';
import {InboxPageModule} from '../pages/inbox/inbox.module';
import {MessagePageModule} from '../pages/message/message.module';
import {ProductdetailsPageModule} from '../pages/productdetails/productdetails.module';
import {CustomizePageModule} from '../pages/customize/customize.module';
import {ShoppingcartPageModule} from '../pages/shoppingcart/shoppingcart.module';
import {OrdernotesPageModule} from '../pages/ordernotes/ordernotes.module';
import {PaymentPageModule} from '../pages/payment/payment.module';
import {SuccessPageModule} from '../pages/success/success.module';
import {AddcardPageModule} from '../pages/addcard/addcard.module';
import {ProductsearchPageModule} from '../pages/productsearch/productsearch.module';
import {UsersProvider} from "../providers/users/users";
import {TabsPage} from "../pages/tabs/tabs";
import {HttpClientModule} from "@angular/common/http";
import {IonicStorageModule} from "@ionic/storage";
import {SelectMoodPageModule} from "../pages/select-mood/select-mood.module";
import {ReviewsPageModule} from "../pages/reviews/reviews.module";
import {ReviewsPage} from "../pages/reviews/reviews";
import {AddMoodPageModule} from "../pages/add-mood/add-mood.module";
import {MoodPageModule} from "../pages/mood/mood.module";
import {MoodItemsPageModule} from "../pages/mood-items/mood-items.module";
import {MoodPage} from "../pages/mood/mood";
import { PostPageModule } from '../pages/post/post.module';



@NgModule({
    declarations: [
        MyApp,
        SigninPage,
        TabsPage,
        HomePage,


    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        MessagePageModule,
        ProductdetailsPageModule,
        SignupPageModule,
        SignupsellPageModule,
        CommentsPageModule,
        InboxPageModule,
        CustomizePageModule,
        ShoppingcartPageModule,
        OrdernotesPageModule,
        PaymentPageModule,
        SuccessPageModule,
        AddcardPageModule,
        ProductsearchPageModule,
        AddMoodPageModule,
        MoodPageModule,
        MoodItemsPageModule,
        SelectMoodPageModule,
        ReviewsPageModule,
        PostPageModule

        IonicModule.forRoot(MyApp,{
            tabsHideOnSubPages :true,
            pageTransition: "ios-transition"
        }),
        IonicStorageModule.forRoot()
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        SigninPage,
        TabsPage,
        ReviewsPage,
        MoodPage

    ],
    providers: [
        StatusBar,
        SplashScreen,
        UsersProvider,
        {provide: ErrorHandler, useClass: IonicErrorHandler}
    ]
})
export class AppModule {
}
